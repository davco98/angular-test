import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

  catalogSelected: Catalog;
  currentIndex: number;
  slideActive = false;
  slideDuration = 3000;
  slideTimer = null;
  catalogs: Array<Catalog> = [
    {
      thumb: '/assets/images/thumb/tea-light-thumb.jpeg',
      image: '/assets/images/tea-light.jpeg'
    },
    {
      thumb: '/assets/images/thumb/white-light-thumb.jpeg',
      image: '/assets/images/white-light.jpeg',
    },
    {
      thumb: '/assets/images/thumb/pink-light-thumb.jpeg',
      image: '/assets/images/pink-light.jpeg',
    },
    {
      thumb: '/assets/images/thumb/tea-light-thumb.jpeg',
      image: '/assets/images/tea-light.jpeg',
    }
  ];

  constructor() { }

  ngOnInit() {
   this.selectedCatalog(0);
  }

  selectedCatalog(index: number) {
    this.currentIndex = index;
    this.catalogSelected = this.catalogs[index];
  }

  previousClick() {
    let newIndex = (this.currentIndex == 0) ? this.catalogs.length-1 : this.currentIndex-1;
    
    this.selectedCatalog(newIndex);
  }

  nextClick() {
    let newIndex = (this.currentIndex == this.catalogs.length-1) ? 0 : this.currentIndex+1;
    
    this.selectedCatalog(newIndex);
  }

  slideChange(checked) {
    this.slideActive = checked;

    if(checked){
      this.slideTimer = setInterval(() => {
        this.nextClick();
      },this.slideDuration);
    }else{
      this.resetSlideTimer();
    }
  }

  resetSlideTimer() {
    clearInterval(this.slideTimer);
  }

}


export class Catalog {
  thumb: string;
  image: string;
}
